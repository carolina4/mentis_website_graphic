import sqlite3
from sqlite3 import Error
import csv
import json
 
 
def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
 
    return None

def get_view(conn, center_name):
    file_name = center_name.replace(" ","_")
    csvWriter = csv.writer(open("static/data/csv/"+file_name+".csv", "w+",  newline=''))
    cur = conn.cursor()
    cur.execute("SELECT DISTINCT  use_case.name AS use_case, data_type.name as data_type, algorithm_type.name as algorithm_type "+
    "FROM algorithm, algorithm_type, data_type, function, industry, objective, Objective_Algo, Objective_TrainingMethod, TechApp_DataType,"+
    " technological_application, training_methodology, type_of_analytics, use_case, UseCase_Industry, UseCase_Objective, UseCase_TechApp, "+
    "UseCase_TypeAnalytics WHERE data_type.id = TechApp_DataType.data_id and TechApp_DataType.tech_id= technological_application.id and "+
    "technological_application.id = UseCase_TechApp.tech_id and UseCase_TechApp.usecase_id = use_case.id and use_case.id = "+
    "UseCase_TypeAnalytics.usecase_id and UseCase_TypeAnalytics.typeanalytics_id = type_of_analytics.id and use_case.id = "+
    "UseCase_Objective.usecase_id and UseCase_Objective.objective_id = objective.id and objective.id = Objective_TrainingMethod.objective_id"+
    " and Objective_TrainingMethod.training_id = training_methodology.id and objective.id = Objective_Algo.objective_id and Objective_Algo.algo_id"+
    " = algorithm.id and algorithm.algo_type = algorithm_type.id and use_case.function = function.id and use_case.id = UseCase_Industry.usecase_id"+
    " and UseCase_Industry.industry_id = industry.id and  use_case.name='"+center_name+"' GROUP BY use_case, data_type, algorithm_type")

    rows = cur.fetchall()
    '''
    csvWriter.writerows(rows)
    '''
    
    #pre process rows
    for row in rows:
        # do your stuff
        row = (row[1],row[2])
        csvWriter.writerow(row)
    


 
def main():
    database = "C:\\Users\\Carolina\\Desktop\\sql_lite_db\\database.db"
    central_nodes_file = "central_nodes_text.txt"

    f = open(central_nodes_file,"r")
    central_nodes = json.loads(f.read())
    f.close()

    # create a database connection
    conn = create_connection(database)
    with conn:
        for node in central_nodes:
            # query view and write it to csv
            get_view(conn, node)
 
if __name__ == '__main__':
    main()