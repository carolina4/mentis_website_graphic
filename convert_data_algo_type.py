import os
import csv
import json 

def __eq__(self, other):
    return self["algo_type"]==other["algo_type"]

def getNodes(fileName):
    nodes = []
    f = open(fileName)
    with f as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        last_algo_type = ""
        for row in csv_reader:
            if last_algo_type!=row[0]:
                nodes.append({"algo_type": row[0], "algo": [] })
                last_algo_type=row[0]

        f.seek(0)
        for row in csv_reader:
            algo_type_elem = next((x for x in nodes if x["algo_type"] == row[0]), None)
            nodes.remove(algo_type_elem)
            algo_type_elem["algo"].append(row[1])
            nodes.append(algo_type_elem)

        return nodes


def saveNodes(nodes, destFilePath):
    f = open(destFilePath,"w+")
    f.write(json.dumps(nodes))
    f.close()
##################################################################################

origFileName = "algo_type_algo.csv"
destFileName = "algo_type_algo.json"

origFilePath = os.path.dirname(os.path.abspath(__file__))+'\\static\\data\\csv\\'+origFileName
destFilePath = os.path.dirname(os.path.abspath(__file__))+'\\static\\data\\json\\'+destFileName

nodes = getNodes(origFilePath)
saveNodes(nodes, destFilePath)


