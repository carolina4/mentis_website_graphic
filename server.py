from flask import Flask, render_template, send_from_directory
import os
app = Flask(__name__)

# FLASK_APP=server.py flask run

'''
@app.route("/connections_chart")
def render_connections_chart():
    template_data = {"data_file_name": "/static/data/data.tsv"}
    return render_template('dendrogram3.html', template_data=template_data)
'''

# finalize building the file name to request new json and do the request 
@app.route("/dag")
def render_graph():
    template_data = {"data_file_name": "/static/data/data.tsv"}
    #return render_template('dag2.html', template_data=template_data) #tabs
    return render_template('dag3.html', template_data=template_data) #drop down

@app.route('/dag_data/<path:path>')
def send_dag_data(path):
    return send_from_directory('static', 'data/json/'+path+'.json')

@app.route('/side_page_data')
def send_algo_type_algo():
    return send_from_directory('static', 'data/json/algo_type_algo.json')