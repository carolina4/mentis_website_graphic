import os
import csv
import json 

def getNodes(fileName):
    nodes = {"innerNodes":[], "outerNodes":[], "links":[]}
    f = open(fileName)
    with f as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        inner_nodes_text = []
        outer_nodes_text = []
        line_count = 0
        for row in csv_reader:
            if line_count != 0:
                inner_nodes_text.append(row[0])
                outer_nodes_text.append(row[1])
            line_count += 1
        
        inner_nodes_text = list(set(inner_nodes_text))
        outer_nodes_text = list(set(outer_nodes_text))

        for index, node in enumerate(inner_nodes_text):
            nodes["innerNodes"].append({"name": str(index), "text": node})

        for index, node in enumerate(outer_nodes_text):
            nodes["outerNodes"].append({"name": str(index+len(inner_nodes_text)), "text": node}) 

        f.seek(0)
        line_count = 0
        for row in csv_reader:
            if line_count != 0:
                link_source = next((x["name"] for x in nodes["innerNodes"] if x["text"] == row[0]), None)
                link_target = next((x["name"] for x in nodes["outerNodes"] if x["text"] == row[1]), None)
                nodes["links"].append({"source": link_source, "target": link_target})
            line_count += 1
        
        return nodes


def saveNodes(nodes, destFilePath):
    f = open(destFilePath,"w+")
    f.write(json.dumps(nodes))
    f.close()
##################################################################################

f = open("central_nodes_text.txt","r")
central_nodes = json.loads(f.read())
f.close()

for node in central_nodes:
    origFileName = node.replace(" ","_")+'.csv'
    graphicTitle = node
    title_size_px = '10px'
    destFileName = graphicTitle.replace(" ","_")+'_dag_data.json'
    image_url = 'static/img/rose.jpg'

    origFilePath = os.path.dirname(os.path.abspath(__file__))+'\\static\\data\\csv\\'+origFileName
    destFilePath = os.path.dirname(os.path.abspath(__file__))+'\\static\\data\\json\\'+destFileName

    nodes = getNodes(origFilePath)
    nodes["title"] = graphicTitle
    nodes["title_size_px"] = title_size_px
    nodes["image_url"] = image_url

    saveNodes(nodes, destFilePath)


